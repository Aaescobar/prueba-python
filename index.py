import csv
from tqdm import tqdm
import networkx as nx

# create graph empty
G = nx.DiGraph()

# looking in the file
with open('./hero-network.csv', 'r') as f:
    data = csv.reader(f)
    headers = next(data)
    for row in tqdm(data):
        G.add_node(row[0]) 
        G.add_node(row[1]) 
        if G.has_edge(row[0], row[1]):
            G[row[0]][row[1]]['weight'] += 1
        else:
            G.add_edge(row[0], row[1], weight = 1)

# Showing the length
G_nodes = G.number_of_nodes()
G_edges = G.number_of_edges()

print("Nodes = ", G_nodes, " Edges = ",G_edges)

# saving file
nx.write_gexf(G, "./hero-network.gexf")
